var fs = require('fs');
var fse = require('fs.extra');
var fstream = require('fstream');
var unzip = require('unzip');

const publicFolder = __dirname + '/../public/';

let extract = (src, dest) => {
    console.log(src);
    console.log(dest);
    let readStream = fs.createReadStream(src);
    readStream
        .pipe(unzip.Extract({ path: dest })).on('close', () => {
            console.log("done");    
        });
}

module.exports = () => {
    fs.readdirSync(publicFolder).forEach((filename) => {

        if (filename.match(/.+\.zip$/)) {
        
            let folder = filename.replace('.zip', '');
            console.log(folder);
        
            fs.stat(publicFolder + folder, function (err, stat) {
                if (err == null) {
                    fse.rmrfSync(publicFolder + folder);
                    fse.mkdirpSync(publicFolder + folder);
                } else if (err.code == 'ENOENT') {
                    fse.mkdirpSync(publicFolder + folder);
                } else {
                    console.log('Some other error: ', err.code);
                }
                extract(publicFolder + filename, publicFolder + folder);
            });
        }
    });
};


