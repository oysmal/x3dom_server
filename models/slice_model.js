const mongoose = require('mongoose');

let schema = new mongoose.Schema({
    name: {type: String, index: {unique: true}},
    description: 'string',
    imageUrl: 'string',
    imageType: 'string',
    position: 'string',
    startPos: 'string',
    endPos: 'string',
    start_e: 'number',
    start_n: 'number',
    end_e: 'number',
    end_n: 'number',
    start_depth: 'number',
    end_depth: 'number',
    related_links: 'array'
});

let SliceModel = mongoose.model('SliceModel', schema);

module.exports = SliceModel;