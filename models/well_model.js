const mongoose = require('mongoose');

let schema = new mongoose.Schema({
    name: {type: String, index: {unique: true}},
    description: 'string',
    positions: [],
    positions_row_length: 'number',
    propertyNames: [],
    propertyValues: [],
    property_values_row_length: 'number',
    related_links: 'array'
});

let WellModel = mongoose.model('WellModel', schema);

module.exports = WellModel;