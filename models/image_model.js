const mongoose = require('mongoose');
const ImageStore = require('./image_store');

let schema = new mongoose.Schema({
    name: { type: String, index: { unique: true } },
    description: String,
    file: { type: mongoose.Schema.ObjectId },
    contentType: String
});


schema.statics.findById = (id, cb) => {
    ImageModel.findOne({ _id: id }).exec((err, data) => {
        if (err) return cb(err);

        ImageStore.readFile(data.name, (err, file) => {
            console.log("BACK!");
            if (err) return cb(err);

            data.file = file;
            return cb(null, data);
        });
    });
};

schema.statics.findFile = (name, cb) => {
    ImageStore.readFile(name, (err, file) => {
        if (err) return cb(err);

        return cb(null, file);
    });
};


schema.statics.findAll = (cb) => {
    ImageModel.find({}).exec((err, data) => {
        if (err) return cb(err);

        let resp = [];

        data.forEach((model) => {
            ImageStore.readFile(model.file, (err, file) => {
                if (err) return cb(err);

                model.file = file;
                resp.push(model);
            });
        });

        cb(null, resp);
    });
};

let ImageModel = mongoose.model('ImageModel', schema);


module.exports = { ImageModel: ImageModel};