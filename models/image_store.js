const mongoose = require('mongoose');
const Grid = require('gridfs-stream');
const GridStore = require('mongodb').GridStore;
const mongodb = require('mongodb');
const stream = require('stream');
Grid.mongo = mongoose.mongo;
let conn = mongoose.connection;


let uploadFile = (file, cb) => {
    var gfs = Grid(conn.db);

    console.log(file.filename + " is being written");
    // streaming to gridfs
    //filename to store in mongodb
    let writestream = gfs.createWriteStream({
        filename: file.filename,
        metadata: {
            type: file.type
        }
    });

    let readstream = new stream.PassThrough();
    readstream.end(file.data);
    readstream.pipe(writestream);

    writestream.on('close', (f) => {
        // do something with `file`
        console.log(f.filename + 'Written To DB');
        return cb(null, f);
    });

    writestream.on('error', (err) => {
        console.log(err + 'Failed to upload file to database');
        return cb(err);
    });
}


let readFile = (filename, cb) => {
    var gfs = Grid(conn.db);

    let buffer = [];

    let readstream = gfs.createReadStream({
        filename: filename
    });

    readstream.on('data', (data) => {
        console.log("got data");
        buffer.push(data);
    });

    readstream.on('close', () => {
        console.log("read " + buffer.length + " chunks");
        return cb(null, Buffer.concat(buffer));
    });

    readstream.on('err', (err) => {
        console.log("Err: " + err);
        return cb(err);
    });
}



module.exports = {readFile: readFile, uploadFile: uploadFile};