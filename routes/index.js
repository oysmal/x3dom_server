var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');

const publicFolder = __dirname + '/../public/';

module.exports = (app) => {

  // Read all directories in public, assume they are unpacked WARS containing SPA's,
  // and set up a path to the filename to redirect to /public/[filename]/index.html
  fs.readdirSync(publicFolder).forEach((filename) => {
    let stats = fs.statSync(publicFolder + filename);
    if(!stats.isDirectory) return;
    let indexPath = filename + '/index.html';

    router.use(express.static(path.join(__dirname + '/public', filename)));

    router.get('/' + filename, (req, res, next) => {
      res.sendFile(indexPath);
    });

  });

  return router;
};
