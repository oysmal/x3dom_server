var express = require('express');
var router = express.Router();
var testModel = require('./test_model')();
var sliceModel = require('./slice_model')();
var well = require('./well')();
var imageModel = require('./image_model');

module.exports = (app) => {

  app.use('/api/test_model', testModel);
  app.use('/api/slice_model', sliceModel);
  app.use('/api/image_model', imageModel(app));
  app.use('/api/well', well);

  /* GET API version listing. */
  router.get('/', function (req, res, next) {
    res.status(200)
      .json({
        apiVersion: '0.1'
      });
  });

  return router;
};