var express = require('express');
var router = express.Router();
var TestModel = require('../../models/test_model');

module.exports = () => {

    router.post('/', (req, res, next) => {
        console.log("Req.body");
        console.log(req.body);
        let x = new TestModel({
            name: req.body.name,
            text: req.body.text
        });
        x.save((err) => {
            if (err) {
                console.log("MONGODB ERROR");
                return res.status(500).send(err);
            }
        });
        return res.status(200).send("OK");

    });

    router.get('/', (req, res, next) => {
        console.log("GET");
        TestModel.find({}).exec((err, data) => {
            if (err) {
                console.log(err);
                return res.status(500);
            }
            console.log("no error");
            res.status(200).json(data);
        });
    });

    return router;
};