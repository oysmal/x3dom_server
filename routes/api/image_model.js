var express = require('express');
var router = express.Router();
var ImageModel = require('../../models/image_model').ImageModel;
var uploadFile = require('../../models/image_model').uploadFile;
var stream = require('stream');
var mongoose = require('mongoose');

// BusBoy Middleware
let busBoyMiddleWare = function (req, res, next) {
    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        console.log(file);
        let b = [];
        file.on('data', (data) => {
            console.log('File [' + fieldname + '] got ' + data.length + ' bytes');
            b.push(data);
        });
        file.on('end', () => {
            if (!req.body) req.body = {};
            req.body.data = Buffer.concat(b);
            next();
        });
    });
    req.busboy.on('field', function (key, value, keyTruncated, valueTruncated) {
        if (!req.body) req.body = {};
        req.body[key] = value;
        //next();
    });
    // etc ... 
};



module.exports = (app) => {



    router.post('/', busBoyMiddleWare, (req, res, next) => {

        console.log("Req.body");
        console.log(req.body);
        console.log(req.files);

        uploadFile({
            filename: req.body.name,
            data: req.body.data,
            contentType: req.body.contentType
        }, (err, file) => {
            if (err) {
                console.log(err);
                return res.status(500).send(err);
            }

            console.log(file);
            let x = new ImageModel({
                name: req.body.name,
                description: req.body.description,
                //data: req.body.data,
                file: file._id,
                contentType: req.body.contentType
            });

            x.save((err) => {
                if (err) {
                    console.log("MONGODB ERROR");
                    return res.status(500).send(err);
                }
            });
            return res.status(200).send("OK");
        })

    });

    router.get('/', (req, res, next) => {
        console.log("GET");
        ImageModel.find({}, (err, data) => {
            if (err) {
                console.log(err);
                return res.status(500);
            }
            console.log(data);
            res.status(200).json(data);
        });
    });


    /*router.get('/:name', (req, res, next) => {
        console.log("GET " + req.params.name);
        ImageModel.findOne({
            name: req.params.name
        }).exec((err, data) => {
            if (err) {
                console.log(err);
                return res.status(500);
            }
            ImageModel.findFile(req.params.name, (err, file) => {
                if (err) {
                    console.log(err);
                    return res.status(500);
                }
                console.log("FILE DATA: ");
                let ret = {
                    file: data,
                    data: file
                };
                console.log(ret);

                console.log("success finding " + req.params.name);
                res.status(200).send(ret);
            });
        });
    });*/

    router.get('/:name', (req, res, next) => {
        console.log("GET " + req.params.name);

        mongoose.connection.db.collection('fs.files').findOne({filename: req.params.name}, (err, data) => {
            
            console.log(data);
            if(err) {
                console.log(err);
                return res.status(500).send(err);
            } 
            if(!data) {
                console.log("NOT found");
                return res.status(404).send("Not found");
            }

            ImageModel.findFile(data.filename, (err, file) => {
                if (err) {
                    console.log(err);
                    return res.status(500);
                }
                console.log(data.filename + ', ' + data.metadata.type);
                res.type(data.metadata.type);

                console.log("success finding " + req.params.name);
                res.status(200).send(file);
            });

        });
    });

    return router;
};