var express = require('express');
var router = express.Router();
var SliceModel = require('../../models/slice_model');
const ImageStore = require('../../models/image_store');
const busBoyMiddleWare = require('./busboy_middleware');

module.exports = () => {

    router.post('/', busBoyMiddleWare, (req, res, next) => {

        console.log("TYPE: " + req.body.type );
        ImageStore.uploadFile({
            filename: req.body.name,
            data: req.body.data,
            type: req.body.type
        }, (err, file) => {
            if (err) {
                console.log(err);
                return res.status(500).send(err);
            }
        
            let x = new SliceModel({
                name: req.body.name,
                imageUrl: "/image_model/"+req.body.name,
                imageType: req.body.type,
                description: req.body.description,
                position: req.body.position,
                startPos: req.body.startPos,
                endPos: req.body.endPos,
                start_e: req.body.start_e,
                start_n: req.body.start_n,
                end_e: req.body.end_e,
                end_n: req.body.end_n,
                start_depth: req.body.start_depth,
                end_depth: req.body.end_depth,
                related_links: req.body.related_links
            });
            x.save((err) => {
                console.log(err);
                if (err) {
                    console.log("MONGODB ERROR");
                    return res.status(500).send(err);
                }
            });
            return res.status(200).send("OK");
        });

    });

    router.get('/', (req, res, next) => {
        console.log("GET");
        SliceModel.find({}).populate('image').exec((err, data) => {
            if (err) {
                console.log(err);
                return res.status(500);
            }
            console.log(data);
            res.status(200).json(data);
        });
    });

    router.get('/:name', (req, res, next) => {
        console.log("GET");
        SliceModel.findOne({name: req.params.name}).exec((err, data) => {
            if (err) {
                console.log(err);
                return res.status(500);
            }
            console.log(data);
            res.status(200).json(data);
        });
    });

    return router;
};