var express = require('express');
var router = express.Router();
var WellModel = require('../../models/well_model');

module.exports = () => {

    router.post('/', (req, res, next) => {
        
            let x = new WellModel({
                name: req.body.name,
                description: req.body.description,
                positions: req.body.positions,
                positions_row_length: req.body.positions_row_length,
                propertyNames: req.body.propertyNames,
                propertyValues: req.body.propertyValues,
                property_values_row_length: req.body.property_values_row_length,
                related_links: req.body.related_links
            });

            x.save((err) => {
                console.log(err);
                if (err) {
                    console.log("MONGODB ERROR");
                    return res.status(500).send(err);
                }

                return res.status(200).json(x);
            });
    });

    router.get('/', (req, res, next) => {
        console.log("GET");
        WellModel.find({}).exec((err, data) => {
            if (err) {
                console.log(err);
                return res.status(500);
            }
            console.log(data);
            res.status(200).json(data);
        });
    });

    router.get('/:name', (req, res, next) => {
        console.log("GET");
        WellModel.findOne({name: req.params.name}).exec((err, data) => {
            if (err) {
                console.log(err);
                return res.status(500);
            }
            console.log(data);
            res.status(200).json(data);
        });
    });

    return router;
};