// BusBoy Middleware
let busBoyMiddleWare = function (req, res, next) {
    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        let b = [];
        file.on('data', (data) => {
            console.log('File [' + fieldname + '] got ' + data.length + ' bytes');
            b.push(data);
        });
        file.on('end', () => {
            if (!req.body) req.body = {};
            req.body.data = Buffer.concat(b);
            next();
        });
    });
    req.busboy.on('field', function (key, value, keyTruncated, valueTruncated) {
        if (!req.body) req.body = {};
        req.body[key] = value;
        //next();
    });
    // etc ... 
};

module.exports = busBoyMiddleWare;